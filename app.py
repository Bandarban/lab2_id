from flask import Flask, render_template
import psycopg2
import os

CONNECT = psycopg2.connect(dbname=os.environ.get("DB_NAME"),
                           user=os.environ.get("DB_USER"),
                           password=os.environ.get("DB_PASS"),
                           host="db",
                           port=5432
                           )
cursor = CONNECT.cursor()
cursor.execute("CREATE TABLE IF NOT EXISTS test (id int);")
cursor.execute("INSERT INTO test VALUES (1), (3), (5)")
CONNECT.commit()
print(f"Connected to {os.environ.get('DBNAME')} database")

app = Flask(__name__)


@app.route('/')
def hello_world():
    cursor = CONNECT.cursor()
    cursor.execute("SELECT * FROM test")
    data = cursor.fetchall()
    return str(data)


if __name__ == '__main__':
    app.run()
